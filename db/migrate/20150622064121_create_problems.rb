class CreateProblems < ActiveRecord::Migration
  def change
    create_table :problems do |t|
      t.string :name
      t.string :email
      t.string :level
      t.text :question

      t.timestamps null: false
    end
  end
end
