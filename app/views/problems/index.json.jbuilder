json.array!(@problems) do |problem|
  json.extract! problem, :id, :name, :email, :level, :question
  json.url problem_url(problem, format: :json)
end
